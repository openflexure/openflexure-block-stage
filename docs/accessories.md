# Block stage accessories

There are a number of accessories you can print for use with the block stage. Unfortunately we have not yet figured out how to print a good V groove, and we recommend you use a commercially available one.

## Light sources

* [CPS532_to_top_plate.stl](models/accessories/CPS532_to_top_plate.stl){previewpage} mounts an 11mm diameter laser diode.
* [LED_to_top_plate.stl](models/accessories/LED_to_top_plate.stl){previewpage} mounts a 5mm LED (e.g. to back-illuminate a target).

## Objective holder

* [rms_to_top_plate.stl](models/accessories/rms_to_top_plate.stl){previewpage} mounts an objective to the stage for fibre coupling.

## Target holder

* [vertical_sample_clips.stl](models/accessories/vertical_sample_clips.stl){previewpage} holds a microscope slide vertically, allowing it to be used as a target to measure motion of the stage (e.g. in conjunction with the LED holder).

## Microscope module

* [microscope_module.stl](models/accessories/microscope_module.stl){previewpage} attaches to the RMS mount, and allows a Raspberry Pi camera module and 12.7mm lens to form a microscope.  This was used for the calibration of drift and precision in the paper. The optics module is essentially equivalent to an OpenFlexure Microscope.

You can [download all of the STLs on this page as a single zipfile](block-stage-accessories.zip){zip, pattern:"*.stl"}
